import axios from "axios";

const API_URL = '/api/vide-greniers/'

//Create a new vide grenier
const createVideGrenier = async (videGrenierData, token) => { 
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }

    const response = await axios.post(API_URL, videGrenierData, config);

    return response.data;
}

// Get vide greniers
const getVideGreniers = async (token) => { 
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    }

    const response = await axios.get(API_URL, config);

    return response.data;
}

const videGrenierService = {
    createVideGrenier,
    getVideGreniers
}

export default videGrenierService;