import {useState, useEffect} from 'react'
import { FaSignInAlt } from 'react-icons/fa'
import { toast } from 'react-toastify'
import { useSelector, useDispatch } from 'react-redux'
import { loginUser, reset } from '../features/auth/authSlice'
import { useNavigate } from 'react-router-dom'
import Spinner from '../components/Spinner'


function Login() {
  useDispatch(reset())
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  })

  const { email, password } = formData
  
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const {user, isLoading, isError, isSuccess, message} = useSelector((state) => state.auth)

  
  const onChange = (e) => { 
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value
    }))
  }
  
  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    
    if (isSuccess || user) {
      navigate('/')
      dispatch(reset())
    }
  }, [user, isError, isSuccess, message, navigate, dispatch])

  const onSubmit = (e) => { 
    e.preventDefault()
    
    const userData = {
      email,
      password
    }

    dispatch(loginUser(userData))
  }

  if (isLoading) { 
    return <Spinner />
  }

  return (
    <>
      <section className='heading'>
        <FaSignInAlt size={50}/> 
        <h1>
          Login
        </h1>
        <p>
          Login to see the latest Garage Sale
        </p>
      </section>

      <section className="form">
        <form onSubmit={onSubmit} className="signup-form">
          <div className="form-input">
            <input type="email"
            className="form-control"
            id="email"
            name="email"
            value={email}
            placeholder="Enter your email"
            onChange={onChange}
            />
            <input type="password"
            className="form-control"
            id="password"
            name="password"
            value={password}
            placeholder="Enter a password"
            onChange={onChange}
            />
          </div>
        <button className="btn">Login</button>
        </form>
      </section>
    </>
  )
}

export default Login