import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import authService from './authService';


// Get user from local storage
const user = JSON.parse(localStorage.getItem('user'));

const initialState = {
    user: user ? user : null,
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: '',
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        reset: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isLoading = false;
            state.message = '';
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(registerUser.pending, (state) => {
                state.isLoading = true
            })
            .addCase(registerUser.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.user = action.payload
            })
            .addCase(registerUser.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
                state.user = null
            })
            .addCase(loginUser.pending, (state) => {
                state.isLoading = true
            })
            .addCase(loginUser.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.user = action.payload
            })
            .addCase(loginUser.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
                state.user = null
            })
            .addCase(logoutUser.fulfilled, (state) => { 
                state.user = null
            })
            .addCase(logoutUser.pending, (state) => { 
                state.isLoading = true
            })
    },
});

// Register user
export const registerUser = createAsyncThunk('auth/registerUser', async (user, thunkAPI) => { 
    try {
        return await authService.registerUser(user);
    } catch (error) {
        const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
        return thunkAPI.rejectWithValue(message);
    }
});

// Login User
export const loginUser = createAsyncThunk('auth/loginUser', async (user, thunkAPI) => { 
    try {
        return await authService.loginUser(user);
    } catch (error) {
        const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
        return thunkAPI.rejectWithValue(message);
    }
});

// Logout User
export const logoutUser = createAsyncThunk('auth/logoutUser', async () => {
        // await authService.logoutUser()
        await localStorage.removeItem("user");
});
// export const logoutUser = async () => {
//     localStorage.removeItem("user");
// }

export const { reset } = authSlice.actions;

export default authSlice.reducer;