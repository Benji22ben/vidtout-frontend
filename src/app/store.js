import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../features/auth/authSlice';
import videGrenierReducer from '../features/videGrenier/videGrenierSlice';

export const store = configureStore({
  reducer: {
    auth: authReducer,
    videGrenier: videGrenierReducer,
  },
});
