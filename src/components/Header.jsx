import {Link, useNavigate} from 'react-router-dom';
import { FaSignInAlt,FaSignOutAlt, FaUser } from 'react-icons/fa';
import { useSelector, useDispatch } from 'react-redux';
import {logoutUser, reset} from '../features/auth/authSlice';

function Header() {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const { user } = useSelector((state) => state.auth)

    const onLogout = () => {
        dispatch(logoutUser())
        dispatch(reset())
        navigate('/login')
    }
    
    return (
    <header>
        <div className="wrapper">
            <Link className='siteTitle' to="/">VidTout</Link>
                <nav>
                    {user ? (
                            <Link to="/login">
                                <button className='logoutBtn' onClick={onLogout}>
                                <FaSignOutAlt />
                                Logout
                                </button>
                            </Link>
                    ) : (
                    <>
                        <Link to="/login">
                            <FaSignInAlt /> Login
                        </Link>
                        <Link to="/signup">
                            <FaUser /> Register
                        </Link>
                    </>
                    )}

            </nav>
        </div>
    </header>
    )
}

export default Header