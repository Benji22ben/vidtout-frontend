import { useState } from 'react';


function VideGrenier({ videGrenier, user }) {
    const [show, setShow] = useState(false);


    const toggle_visibility = () => {
        setShow(!show);
    }

    return (
        <div className="videGrenier" onClick={toggle_visibility}>
            <p>Du {new Date(videGrenier.startDate).toLocaleDateString()} au {new Date(videGrenier.endDate).toLocaleDateString()}</p>
            <h2>{videGrenier.name} - { videGrenier.location }</h2>
            <p>{videGrenier.tags}</p>
            {show && <p id="desc">{videGrenier.description}</p>}
            <p>Créer par {user.user.name}</p>
        </div>
  )
}

export default VideGrenier