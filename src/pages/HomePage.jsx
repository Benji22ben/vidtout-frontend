import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import VideGrenierForm from '../components/VideGrenierForm'
import Spinner from '../components/Spinner'
import { getVideGreniers, reset } from '../features/videGrenier/videGrenierSlice'
import VideGrenierItem from '../components/VideGrenier'

function HomePage() {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { user } = useSelector((state) => state.auth)
  const { videGrenier, isLoading, isError, message } = useSelector((state) => state.videGrenier)


  useEffect(() => {
    if (isError) {
      console.log(message)
    } else {
      dispatch(reset());
    }

    if (user) {
      dispatch(getVideGreniers())
    } else {
      navigate('/login')
    }

    return () => {
      dispatch(reset())
    }
  }, [user, navigate, isError, message, dispatch])

  if (isLoading) {
    return <Spinner />
  }

  return (
    <>
      <section className="heading">
        <h1>Welcome to VidTout</h1>
      </section>

      <VideGrenierForm />

      <section className="content">
        {videGrenier.length > 0 ? (
          <div className='videGreniers'>
            {videGrenier.map((videGrenier) => (
              <VideGrenierItem key={videGrenier._id} videGrenier={videGrenier} user={user}/>
            ))}
          </div>
        ) :  <p>Aucun vide grenier</p>}
      </section>
    </>
  )
}

export default HomePage