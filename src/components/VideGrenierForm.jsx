import { useState } from "react"
import { useSelector, useDispatch } from "react-redux"
import { createVideGrenier } from "../features/videGrenier/videGrenierSlice"

function VideGrenierForm() {
    const [formData, setFormData] = useState({
        name: "",
        location: "", 
        description: "",
        tags: ["#tag1", "#tag2", "#tag3"],
        startDate: "",
        endDate: "",
    })
    const { name, location, description, startDate, endDate } = formData
        console.log("1 " + formData.startDate + " " + formData.endDate)


    const onChange = (e) => { 
        setFormData((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value
        }))
    }
  
    const dispatch = useDispatch()

    const onSubmit = (e) => {
        e.preventDefault()
        const formData = {
            name,
            location,
            description,
            startDate,
            endDate
        }
        dispatch(createVideGrenier(formData))
    }

    return (
        <section className="form">
            <form onSubmit={onSubmit}>
                <div className="form-input">
                    <label>Name</label>
                    <input type="text" name="name" id="name" value={name} onChange={onChange}/>
                </div>
                <div className="form-input">
                    <label>Location</label>
                    <input type="text" name="location" id="location" value={location} onChange={onChange}/>
                </div>
                <div className="form-input">
                    <label>Description</label>
                    <input type="text" name="description" id="description" value={description} onChange={onChange}/>
                </div>
                <div className="form-input">
                    <label>Début</label>
                    <input type="date" name="startDate" id="startDate" value={startDate} onChange={onChange}/>
                </div>
                <div className="form-input">
                    <label>Fin</label>
                    <input type="date" name="endDate" id="endDate" value={endDate} onChange={onChange}/>
                </div>
                <button className="btn" type="submit">Create a new VideGrenier</button> 
            </form>
      </section>
  )
}

export default VideGrenierForm