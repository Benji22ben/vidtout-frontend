import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import videGrenierService from "./videGrenierService";

const initialState = {
    videGrenier: [],
    isError: false,
    isSuccess: false,
    isLoading: false,
    message: ""
}

export const videGrenierSlice = createSlice({
    name: "videGrenier",
    initialState,
    reducers: {
        reset: (state) => initialState
    },
    extraReducers: (builder) => { 
        builder
            .addCase(createVideGrenier.pending, (state) => {
                state.isLoading = true
            })
            .addCase(createVideGrenier.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.videGrenier.push(action.payload)
            })
            .addCase(createVideGrenier.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
            .addCase(getVideGreniers.pending, (state) => {
                state.isLoading = true
            })
            .addCase(getVideGreniers.fulfilled, (state, action) => {
                state.isLoading = false
                state.isSuccess = true
                state.videGrenier = action.payload
            })
            .addCase(getVideGreniers.rejected, (state, action) => {
                state.isLoading = false
                state.isError = true
                state.message = action.payload
            })
    }
});

// Create new videGrenier
export const createVideGrenier = createAsyncThunk('videGrenier/create', async (videGrenierData, thunkAPI) => {
    try {
        const token = thunkAPI.getState().auth.user.token;


        return await videGrenierService.createVideGrenier(videGrenierData, token);
    } catch (error) {
        const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
        return thunkAPI.rejectWithValue(message);
    }
});

// Get videGreniers
export const getVideGreniers = createAsyncThunk('videGrenier/getAll', async (_, thunkAPI) => { 
    try {
        const token = thunkAPI.getState().auth.user.token;

        return await videGrenierService.getVideGreniers(token);
    } catch (error) {
        const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
        return thunkAPI.rejectWithValue(message);
    }
});


export const { reset } = videGrenierSlice.actions;

export default videGrenierSlice.reducer;