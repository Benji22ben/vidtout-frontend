import { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { registerUser, reset } from '../features/auth/authSlice'
import { FaUser } from 'react-icons/fa'
import Spinner from '../components/Spinner'

function SignUp() {
  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    password2: ''
  })

  const { name, email, password, password2 } = formData
  
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const {user, isLoading, isError, isSuccess, message} = useSelector((state) => state.auth)
  
  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    
    if (isSuccess || user) {
      navigate('/')
      dispatch(reset())
    }
  }, [user, isError, isSuccess, message, navigate, dispatch])

  const onChange = (e) => { 
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value
    }))
  }
  
  const onSubmit = (e) => { 
    e.preventDefault()

    if (password !== password2) {
      toast.error('Passwords do not match')
    } else {
      const userData = {
        name,
        email,
        password
      }
      dispatch(registerUser(userData))
    }
  }

  if (isLoading) { 
    return <Spinner />
  }

  return (
    <>
      <section className='heading'>
        <FaUser size={50}/>
        <h1>
          Sign Up
        </h1>
        <p>
          Sign Up to make your own sale or acquisitions
        </p>
      </section>

      <section className="form">
        <form onSubmit={onSubmit} className="signup-form">
          <div className="form-input">
            <input type="text"
            className="form-control"
            id="name"
            name="name"
            value={name}
            placeholder="Enter your name"
            onChange={onChange}
            />
            <input type="email"
            className="form-control"
            id="email"
            name="email"
            value={email}
            placeholder="Enter your email"
            onChange={onChange}
            />
            <input type="password"
            className="form-control"
            id="password"
            name="password"
            value={password}
            placeholder="Enter a password"
            onChange={onChange}
            />
            <input type="password"
            className="form-control"
            id="password2"
            name="password2"
            value={password2}
            placeholder="Confirm password"
            onChange={onChange}
            />
          </div>
          <button className="btn">Sign Up</button>
        </form>
      </section>
    </>
  )
}

export default SignUp